import http from 'http';
import Koa from 'koa';
import api from './api/index.api';
import Utility from './common/Utility';
import middleware from './middleware';
import koaStatic from 'koa-static';
import path from 'path';
import socketio from 'socket.io';
import cors from 'koa-cors';

const initStart = async () => {
  try {
    const port = process.env.PORT || 8080;
    Utility.printLog('init start port:', port);
    const app = new Koa();
    app.use(koaStatic(path.resolve(__dirname, '../public')));
    app.keys = ['secret'];
    // app.use(middleware());
    app.use(
      cors({
        origin: function (ctx) {
          console.log('---origin--');
          return ctx.req.origin;
        },
      }),
    );
    app.use(api());
    app.use(async (ctx) => {
      ctx.status = 404;
      ctx.body = { code: 404, msg: '未找到' };
    });
    app.context.onerror = function (err) {
      if (!err) {
        return;
      }
      Utility.printLog(err);
      const { message } = err || {};

      try {
        this.status = err.status || 500;
        this.body = { msg: message };
        this.res.end(JSON.stringify(this.body));
      } catch (ex) {
        Utility.printLog(ex);
      }
    };

    const server = http.createServer(app.callback()).listen(port);

    const io = socketio(server, { path: '/chat', cors: { origin: '*' } });
    const socketMap = {};
    io.on('connection', (socket) => {
      console.log('------------------', io.engine.clientsCount, socket.id);
      // console.log(a, b, c);
      socketMap[socket.id] = socket;
      socket.on('msg', (data) => {
        console.log('data-->', data);
        // socket.emit('msg', { msg: data, ts: new Date().getTime() });
        Object.values(socketMap).forEach((client) => {
          client.emit('msg', { id: client.id, msg: data.msg, ts: new Date().getTime() });
        });
      });

      socket.on('createRoom', (data) => {
        console.log('------1--createRoom-----', data);
        if (data) {
          console.log('room name:', data.name);
          socket.join(data.name);
        }
      });
      socket.on('joinRoom', (data) => {
        console.log('------2---joinRoom----', data);
        socket.join(data.name);
      });
      socket.on('sendRoomMsg', (data) => {
        console.log('------3---sendRoomMsg----', data);
        socket.to(data.name).emit('msg', data.msg);
      });
    });

    process.on('SIGINT', () => {
      server.close(() => {
        setTimeout(() => {
          process.exit(0);
        }, 300);
      });
    });
  } catch (ex) {
    console.log(ex);
  }
};

initStart();

process.on('unhandledRejection', (reason, p) => {
  Utility.printLog(reason, p);
});

// import express from 'express';
// import mongoose from 'mongoose';
// import path from 'path';
// // import cfg from './cfg';
// // import UserService from './service/UserService';

// const app = express();
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// // const router = require('./api/index');

// // set static directory
// app.use(express.static(path.join(__dirname, 'public')));

// // Cross domain processing
// app.use(async (req, res, next) => {
//   const { method, url } = req;
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization,token, Accept, X-Requested-With');
//   res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
//   console.log(method, url);
//   if (req.method == 'OPTIONS') {
//     res.sendStatus(200);
//   } else {
//     await next();
//   }
// });

// // set api router
// // router(app);
// app.get('/api/test', (request, response) => {
//   try {
//     response.json({ method: request.header.method, ts: new Date().getTime() });
//   } catch (ex) {
//     response.status(400).json({ msg: ex.message });
//   }
// });

// const server = require('http').createServer(app);
// const port = process.env.PORT || 4100;
// server.listen(port, () => {
//   console.log(`http://localhost:${port}`);
// });

// const io = socketio(server, { path: '/chat', cors: { origin: '*' } });
// io.on('connection', (socket) => {
//   console.log('------------------', io.engine.clientsCount, socket.id);
//   // console.log(a, b, c);

//   socket.on('msg', (data) => {
//     console.log('data-->', data);
//   });
// });
